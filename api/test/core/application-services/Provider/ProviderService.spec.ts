import 'mocha';
import container                                from '../../../../inversify.config';
import ProviderServiceContract                  from '../../../../src/core/application-services/Provider/IProviderService';
import {ProviderService}                        from '../../../../src/core/application-services/Provider/ProviderService';
import {DOMAIN_APPLICATION_SERVICE_IDENTIFIERS} from '../../../../src/core/CoreModuleSymbols';
import {Provider}                               from '../../../../src/core/domain/Provider/Provider';

const assert = require('Chai').assert;

describe(`ProviderService`, function () {
  let providerService: ProviderServiceContract;

  before(async function before() {
    providerService = container.get<ProviderService>(DOMAIN_APPLICATION_SERVICE_IDENTIFIERS.PROVIDER_SERVICE);
  });

  it(`should be instance of ${ProviderService.name}`, function () {
    assert.instanceOf(providerService, ProviderService);
  });

  it(`should find a ${Provider.name}`, function () {
    const name               = 'gas';
    const provider: Provider = providerService.findProvider(name);
    assert.instanceOf(provider, Provider);
  });

  it(`should find [${Provider.name}]`, function () {
    const names: any            = ['gas', 'internet'];
    const providers: [Provider] = providerService.findProviders(names);
    assert.typeOf(providers, 'array');
    providers.forEach((provider) => {
      assert.instanceOf(provider, Provider);
    });
  });

});
