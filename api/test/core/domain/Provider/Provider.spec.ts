import 'mocha';
import {Provider} from '../../../../src/core/domain/Provider/Provider';

const assert = require('Chai').assert;

describe('Provider', function () {

  const name     = 'gas';
  const endpoint = 'http://localhost/api/v1/test/gas';
  let provider: Provider;

  before(async function before() {
    provider = new Provider(name, endpoint);
  });

  it(`should be instance of ${Provider.name}`, function () {
    assert.instanceOf(provider, Provider);
  });

  it('should have a name', function () {
    assert.equal(provider.name, name);
    assert.equal(provider.endpoint, endpoint);
  });

  it('should have an endpoint', function () {
    assert.equal(provider.endpoint, endpoint);
  });

});
