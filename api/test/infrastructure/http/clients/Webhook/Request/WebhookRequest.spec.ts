import 'mocha';
import {WebhookRequest} from '../../../../../../src/infrastructure/http/clients/Webhook/Request/WebhookRequest';

const assert = require('Chai').assert;

describe('WebhookRequest', function () {

  const callbackUrl = 'http://localhost:3000/providers/webhook:mock';

  let request: WebhookRequest;

  before(async function before() {
    request = new WebhookRequest(callbackUrl);
  });

  it(`should be instance of ${WebhookRequest.name}`, function () {
    assert.instanceOf(request, WebhookRequest);
  });

  it('should have a callbackUrl', function () {
    assert.equal(request.callbackUrl, callbackUrl);
  });

});
