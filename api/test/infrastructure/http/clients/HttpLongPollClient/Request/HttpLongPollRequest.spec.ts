import 'mocha';
import {HttpLongPollRequest}  from '../../../../../../src/infrastructure/http/clients/HttpLongPoll/Request/HttpLongPollRequest';
import {HttpLongPollResponse} from '../../../../../../src/infrastructure/http/clients/HttpLongPoll/Response/HttpLongPollResponse';

const assert = require('Chai').assert;

describe('HttpLongPollRequest', () => {
  const method   = 'get';
  const endpoint = 'http://localhost:3000/providers/internet';
  let request: HttpLongPollRequest;

  before(async function before() {
    request = new HttpLongPollRequest(endpoint);
  });

  it(`should be instance of ${HttpLongPollRequest.name}`, function () {
    assert.instanceOf(request, HttpLongPollRequest);
  });

  it('should have a method', function () {
    assert.equal(request.method, method);
  });

  it('should have an endpoint', function () {
    assert.equal(request.endpoint, endpoint);
  });

  it(`should generate a handshake which returns a promise for an instance of ${HttpLongPollResponse.name} [when sent]`, function () {
    const handshake = request.send();
    const promise   = handshake.next().value;
    assert.instanceOf(promise, Promise);
    promise.then(async function (response: HttpLongPollResponse) {
      assert.instanceOf(response, HttpLongPollResponse);
    });
  });

});
