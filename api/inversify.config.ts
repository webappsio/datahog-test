import 'reflect-metadata';
import {AppContainer} from './src/dependency/AppContainer';

let container = new AppContainer();
container.init();
export default container;

