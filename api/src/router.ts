import express = require('express');
import {Request, Response}                      from 'express';
import container                                from '../inversify.config';
import {ProviderWebhookService}                 from './core/application-services/Provider/Webhook/ProviderWebhookService';
import {DOMAIN_APPLICATION_SERVICE_IDENTIFIERS} from './core/CoreModuleSymbols';
import {Firewall}                               from './web/middleware/Firewall/Firewall';

const HttpStatus = require('http-status-codes');

const router = express.Router();

const providerWebhookService = container.get<ProviderWebhookService>(DOMAIN_APPLICATION_SERVICE_IDENTIFIERS.PROVIDER_WEBHOOK_SERVICE);

router.post('/', Firewall.requireJson(), (request: Request, response: Response) => {
  try {
    providerWebhookService.create(request.body.providers, request.body.callbackUrl);
    response
      .status(HttpStatus.OK)
      .json({
              providers: request.body.providers,
              success  : true
            });
  } catch (error) {
    response.status(HttpStatus.INTERNAL_SERVER_ERROR0)
      .json({error: error.toString()});
  }
});
export = router;
