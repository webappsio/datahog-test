import {injectable} from 'inversify';

const nodeFetch = require('node-fetch');

@injectable()
export class HttpClient {

  async get(url: string): Promise<any> {
    console.debug(`🎣 ${HttpClient.name} (GET) request...`);
    return nodeFetch(url, {
      method: 'get'
    })
      .then((response: any) => {
        if (!response.ok) {
          return Promise.reject(`🎣 ${HttpClient.name} (GET) response not OK`);
        }
        return response.json();
      })
      .catch((error: any) => {
        console.error(error);
        return null;
      });
  }

  async post(url: string, data: any): Promise<any> {
    console.debug(`📬 ${HttpClient.name} (POST) request...`);
    console.table({
                    Url      : url,
                    Data     : data
                  });
    nodeFetch(url, {
      method : 'post',
      body   : JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
    })
      .then((response: any) => {
        /** @todo Retry failed webhook callbacks [not required by spec] */
        if (!response.ok) {
          throw new Error('📬 Post response not OK');
        }
        console.info('📬 Post response status', response.status);
        const json = response.json();
        return json;
      })
      .catch((error: any) => {
        console.error(error);
      });
  }
}

export default HttpClient;
