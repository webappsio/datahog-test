import {HttpLongPollResponse} from '../Response/HttpLongPollResponse';
import IHttpLongPollRequest   from './IHttpLongPollRequest';

const nodeFetch = require('node-fetch');

export class HttpLongPollRequest implements IHttpLongPollRequest {
  constructor(public readonly endpoint: string, public readonly method: string = 'get') {
    this.endpoint = endpoint;
    this.method   = method;
  }
  public *send(): Generator<any> {
    while (true) {
      yield nodeFetch(this.endpoint, {
        method: this.method
      })
        .then((response: any) => {
          const ok = response.ok;
          try {
            const json = response.json();
            return new HttpLongPollResponse(ok, json);
          } catch (error) {
            console.error(error);
            throw error;
            return new HttpLongPollResponse(false, null);
          }
        })
        .catch((error: any) => {
          console.error(error);
          return null;
        });
    }
  }
}
