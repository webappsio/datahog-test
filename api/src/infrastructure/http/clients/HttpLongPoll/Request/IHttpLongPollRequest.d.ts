export default interface IHttpLongPollRequest {
  send(): Generator<any>;
}
