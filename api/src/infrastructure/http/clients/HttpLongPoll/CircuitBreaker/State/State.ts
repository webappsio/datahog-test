enum STATE {
  OPEN   = 'OPEN',
  CLOSED = 'CLOSED',
  HALF   = 'HALF'
}

export default STATE;
