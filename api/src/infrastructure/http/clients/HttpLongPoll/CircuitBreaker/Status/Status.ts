enum STATUS {
  SUCCESS   = 'Success',
  FAILURE = 'Failure',
}

export default STATUS;
