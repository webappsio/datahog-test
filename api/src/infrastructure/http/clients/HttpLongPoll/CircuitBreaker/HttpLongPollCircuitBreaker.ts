import {injectable}            from 'inversify';
import IHttpLongPollRequest    from '../Request/IHttpLongPollRequest';
import {HttpLongPollResponse}  from '../Response/HttpLongPollResponse';
import STATE                   from './State/State';
import STATUS                  from './Status/Status';
import {CircuitBreakerAttempt} from './types/CircuitBreakerAttempt';
import {CircuitBreakerTimeout} from './types/CircuitBreakerTimeout';

@injectable()
export class HttpLongPollCircuitBreaker {

  state: STATE;
  failureThreshold: number;
  failureCount: number;
  successThreshold: number;
  successCount: number;
  timeout: CircuitBreakerTimeout;
  nextAttempt: CircuitBreakerAttempt;

  constructor() {
    this.state            = STATE.OPEN;
    this.failureThreshold = 3;
    this.failureCount     = 0;
    this.successThreshold = 2;
    this.successCount     = 0;
    this.timeout          = 6000;
    this.nextAttempt      = Date.now();
  }

  async fire(request: IHttpLongPollRequest): Promise<any> {
    if (this.state === STATE.OPEN) {
      if (this.nextAttempt <= Date.now()) {
        this.state = STATE.HALF;
      } else {
        throw new Error(`🔋 ${HttpLongPollCircuitBreaker.name} is ${STATE.OPEN}.`);
      }
    }
    const handshake = await request.send();
    return this.success(handshake.next()
                          .value
                          .then(async (response: HttpLongPollResponse) => {
                            return (response && response.ok) ? response.data : Promise.reject(`🔋 ${HttpLongPollCircuitBreaker.name} rejected bad server response.`);
                          })
                          .catch((error: any) => {
                            this.fail(error);
                            throw error;
                          }));
  }

  success(response: any) {
    if (this.state === STATE.HALF) {
      this.successCount++;
      if (this.successCount > this.successThreshold) {
        this.successCount = 0;
        this.state        = STATE.CLOSED;
      }
    }
    this.failureCount = 0;

    this.status(STATUS.SUCCESS);
    return response;
  }

  fail(err: any) {
    this.failureCount++;
    if (this.failureCount >= this.failureThreshold) {
      this.state       = STATE.OPEN;
      this.nextAttempt = Date.now() + this.timeout;
    }
    this.status(STATUS.FAILURE);
    return err;
  }

  status(action: any) {
    console.debug(`🔋 ${HttpLongPollCircuitBreaker.name} status updated.`);
    console.table({
                    Action      : action,
                    Timestamp   : Date.now(),
                    Successes   : this.successCount,
                    Failures    : this.failureCount,
                    'Next State': this.state
                  });
  }
}
