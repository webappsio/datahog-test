import IHttpLongPollResponse from './IHttpLongPollResponse';

export class HttpLongPollResponse implements IHttpLongPollResponse {
  constructor(public readonly _ok: boolean, public readonly json: any) {
  }
  public get ok(): boolean {
    if (!this._ok) {
      return false;
    }
    const json = this.json;
    if (!json || Array.isArray(json) && json.length < 1) {
      return false;
    }
    return true;
  }
  public get data(): any {
    return this.json;
  }
}
