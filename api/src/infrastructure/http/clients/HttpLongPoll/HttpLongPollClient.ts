import {inject, injectable}         from 'inversify';
import {INFRASTRUCTURE_IDENTIFIERS} from '../../../InfrastructureModuleSymbols';
import {HttpLongPollCircuitBreaker} from './CircuitBreaker/HttpLongPollCircuitBreaker';
import {HttpLongPollRequest}        from './Request/HttpLongPollRequest';

@injectable()
export class HttpLongPollClient {
  constructor(
    @inject(INFRASTRUCTURE_IDENTIFIERS.HTTP_LONG_POLL_CIRCUIT_BREAKER) public readonly circuitBreaker: HttpLongPollCircuitBreaker
  ) {
  }

  public async longPoll(endpoint: string, retries: number = 0): Promise<any> {
    const request  = new HttpLongPollRequest(endpoint);
    const response = this.circuitBreaker
      .fire(request)
      .then((response: any) => {
        console.debug(`💈 ${HttpLongPollClient.name} completed successfully.`);
        console.table({
                        Response: response
                      });
        return Promise.resolve(response);
      })
      .catch((error) => {
        console.error(`💈 ${HttpLongPollClient.name} error:`, error);
        const delay = 1000;
        return this.pause(delay)
          .then(async () => this.retry(retries, endpoint));
      });
    return Promise.resolve(response);
  }

  private async pause(duration: number): Promise<any> {
    console.debug(`💈 ${HttpLongPollClient.name} is pausing`);
    console.table({
                    Duration: duration
                  });
    return new Promise(response => setTimeout(response, duration));
  }

  private async retry(retries: number, url: string): Promise<any> {
    const retry = retries + 1;
    console.debug(`💈 ${HttpLongPollClient.name} is preparing retry request...`);
    console.table({
                    Retries: retry
                  });
    return this.longPoll(url, retry);
  }
}
