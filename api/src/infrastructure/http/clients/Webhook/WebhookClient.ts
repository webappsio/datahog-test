import {inject, injectable}         from 'inversify';
import {INFRASTRUCTURE_IDENTIFIERS} from '../../../InfrastructureModuleSymbols';
import {HttpClient}                 from '../Http/HttpClient';
import {HttpLongPollClient}         from '../HttpLongPoll/HttpLongPollClient';
import {IWebhookEvent}              from './Event/IWebhookEvent';
import {WebhookRequestValidator}    from './Request/Validator/WebhookRequestValidator';
import {WebhookRequest}             from './Request/WebhookRequest';
import {WebhookSubscription}        from './Subscription/WebhookSubscription';

@injectable()
export class WebhookClient {

  constructor(
    @inject(INFRASTRUCTURE_IDENTIFIERS.HTTP_CLIENT) public readonly httpClient: HttpClient,
    @inject(INFRASTRUCTURE_IDENTIFIERS.HTTP_LONG_POLL_CLIENT) public readonly httpLongPollClient: HttpLongPollClient
  ) {
    this.httpLongPollClient = httpLongPollClient;
  }

  async create(events: [IWebhookEvent], callbackUrl: string): Promise<any> {
    return this.subscribe(events, callbackUrl);
  }

  private async subscribe(events: [IWebhookEvent], callbackUrl: string): Promise<any> {
    try {
      const request = this.createRequest(callbackUrl);
      console.debug(`🕸️ ${WebhookClient.name} created webhook request.`);
      console.table({
                      Request: request
                    });
      const subscription = await new WebhookSubscription(this.httpLongPollClient, events);
      console.debug(`🕸️ ${WebhookClient.name} created webhook subscription.`);
      console.table({
                      Request: request
                    });
      return await this.publish(request.callbackUrl, subscription);
    } catch (e) {
      console.error(`🕸️ ${WebhookClient.name} failed to add events to webhook subscription`, e);
    }
  }

  async publish(callbackUrl: string, data: any): Promise<any> {
    return this.httpClient.post(callbackUrl, data);
  }

  private createRequest(callbackUrl: string): WebhookRequest {
    const request = new WebhookRequest(callbackUrl);
    return WebhookRequestValidator.validate(request);
  }
}
