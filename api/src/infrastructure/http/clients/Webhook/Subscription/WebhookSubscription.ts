import {HttpLongPollClient} from '../../HttpLongPoll/HttpLongPollClient';
import {IWebhookEvent}      from '../Event/IWebhookEvent';

export class WebhookSubscription {

  subscription: any;

  constructor(public readonly client: HttpLongPollClient, public readonly event: [IWebhookEvent]) {
    this.create(client, event)
      .then((subscription => {
        this.subscription = subscription;
      }));
  }

  public async create(client: HttpLongPollClient, subscribers: [IWebhookEvent]): Promise<any> {
    return subscribers.reduce(async (accumulator, subscriber): Promise<any> => {
      let data: any         = await accumulator;
      data[subscriber.name] = await client.longPoll(subscriber.endpoint);
      return data;
    }, Promise.resolve([]));
  }
}
