import {WebhookRequest} from "../../WebhookRequest";

export class InvalidWebhookCallbackUrlError extends Error {
  constructor(request: WebhookRequest) {
    super(`Invalid webhook callback url: '${request.callbackUrl}'`);
    this.name = 'InvalidWebhookCallbackUrlError';
  }
}

