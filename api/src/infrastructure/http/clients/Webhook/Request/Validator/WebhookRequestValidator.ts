import {WebhookRequest}                 from '../WebhookRequest';
import {InvalidWebhookCallbackUrlError} from './Error/InvalidWebhookCallbackUrlError';

const validUrl = require('valid-url');

export class WebhookRequestValidator {
  public static validate(request: WebhookRequest): WebhookRequest {
    if (!validUrl.isWebUri(request.callbackUrl)) {
      throw new InvalidWebhookCallbackUrlError(request);
    }
    return request;
  }
}
