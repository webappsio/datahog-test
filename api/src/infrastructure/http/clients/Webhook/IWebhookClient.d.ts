import {IWebhookEvent} from './Event/IWebhookEvent';

export default interface IWebhookClient {
  async
  create(subscribers: [IWebhookEvent],
            callbackUrl: string): Promise<any>;
}
