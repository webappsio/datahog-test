export const INFRASTRUCTURE_IDENTIFIERS = {
  HTTP_CLIENT                   : Symbol.for('HttpClient'),
  HTTP_LONG_POLL_CIRCUIT_BREAKER: Symbol.for('HttpLongPollCircuitBreaker'),
  HTTP_LONG_POLL_CLIENT         : Symbol.for('HttpLongPollClient'),
  WEBHOOK_CLIENT                : Symbol.for('WebhookClient')
};
