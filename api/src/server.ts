import express, {NextFunction, Request, Response} from 'express';
import bodyParser = require('body-parser');
import logger = require('morgan');
import router = require('./router');

function createServer(port: number = 9000) {
  const server = express();
  server.use(logger('dev'));
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({extended: false}));
  server.use(bodyParser.raw());
  server.use('/', router);
  server.use((req: Request, res: Response, next: NextFunction): any => {
    const err: any = new Error('Not Found');
    err['status']  = 404;
    next(err);
  });
  if (server.get('env') === 'development') {
    server.use((err: any, req: Request, res: Response, next: NextFunction): any => {
      res.status(err['status'] || 500);
      res.render('error', {
        message: err.message,
        error  : err
      });
    });
  }
  server.use((err: any, req: Request, res: Response, next: NextFunction): any => {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error  : {}
    });
  });
  server.listen(port, err => {
    if (err) {
      return console.error(err);
    }
    return console.log(`server is listening on ${port}`);
  });
}

createServer();
