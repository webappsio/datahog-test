import {Request, Response} from 'express';

export class Firewall {
  public static requireContentType(contentType: string) {
    return (req: Request, res: Response, next: any) => {
      if (req.headers['content-type'] !== contentType) {
        res.status(400)
          .send(`Server requires ${contentType}`);
      } else {
        next();
      }
    };
  }

  public static requireJson() {
    return this.requireContentType('application/json');
  }
}
