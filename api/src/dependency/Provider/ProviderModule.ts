import {interfaces}                             from 'inversify';
import {ProviderService}                        from '../../core/application-services/Provider/ProviderService';
import {ProviderWebhookService}                 from '../../core/application-services/Provider/Webhook/ProviderWebhookService';
import {DOMAIN_APPLICATION_SERVICE_IDENTIFIERS} from '../../core/CoreModuleSymbols';
import {BaseModule}                             from '../BaseModule';

export default class ProviderModule extends BaseModule {
  constructor(public readonly providers: any) {
    super((bind: interfaces.Bind): void => {
      this.init(bind);
    });
  }

  public init(bind: interfaces.Bind): void {
    this.provideProviderConfig(bind);
    this.provideProviderService(bind);
    this.provideProviderWebhookService(bind);
  }

  private provideProviderConfig(bind: interfaces.Bind): void {
    bind<string>(DOMAIN_APPLICATION_SERVICE_IDENTIFIERS.PROVIDER_SERVICE_CONFIG)
      .toConstantValue(this.providers);
  }

  private provideProviderService(bind: interfaces.Bind): void {
    bind<ProviderService>(DOMAIN_APPLICATION_SERVICE_IDENTIFIERS.PROVIDER_SERVICE)
      .to(ProviderService)
      .inSingletonScope();
  }

  private provideProviderWebhookService(bind: interfaces.Bind): void {
    bind<ProviderWebhookService>(DOMAIN_APPLICATION_SERVICE_IDENTIFIERS.PROVIDER_WEBHOOK_SERVICE)
      .to(ProviderWebhookService)
      .inSingletonScope();
  }
}
