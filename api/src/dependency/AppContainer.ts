import HttpClient                   from '../infrastructure/http/clients/Http/HttpClient';
import {HttpLongPollCircuitBreaker} from '../infrastructure/http/clients/HttpLongPoll/CircuitBreaker/HttpLongPollCircuitBreaker';
import {HttpLongPollClient}         from '../infrastructure/http/clients/HttpLongPoll/HttpLongPollClient';
import {WebhookClient}              from '../infrastructure/http/clients/Webhook/WebhookClient';
import {INFRASTRUCTURE_IDENTIFIERS} from '../infrastructure/InfrastructureModuleSymbols';
import {BaseContainer}              from './BaseContainer';
import ProviderModule               from './Provider/ProviderModule';

const config = require('../../config.json');

export class AppContainer extends BaseContainer {
  init(): void {
    this.provideProviderModule();
    this.provideWebhookClient();
    this.provideHttpClient();
    this.provideHttpLongPollCircuitBreaker();
    this.provideHttpLongPollClient();
  }

  private provideProviderModule(): void {
    this.load(new ProviderModule(config.providers));
  }

  private provideWebhookClient(): void {
    this.bind<WebhookClient>(INFRASTRUCTURE_IDENTIFIERS.WEBHOOK_CLIENT)
      .to(WebhookClient)
      .inSingletonScope();
  }
  private provideHttpClient(): void {
    this.bind<HttpClient>(INFRASTRUCTURE_IDENTIFIERS.HTTP_CLIENT)
      .to(HttpClient)
      .inSingletonScope();
  }
  private provideHttpLongPollCircuitBreaker(): void {
    this.bind<HttpLongPollCircuitBreaker>(INFRASTRUCTURE_IDENTIFIERS.HTTP_LONG_POLL_CIRCUIT_BREAKER)
      .to(HttpLongPollCircuitBreaker);
  }
  private provideHttpLongPollClient(): void {
    this.bind<HttpLongPollClient>(INFRASTRUCTURE_IDENTIFIERS.HTTP_LONG_POLL_CLIENT)
      .to(HttpLongPollClient)
      .inSingletonScope();
  }
}
