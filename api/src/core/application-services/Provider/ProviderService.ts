import {inject, injectable}                     from 'inversify';
import IWebhookClient                           from '../../../infrastructure/http/clients/Webhook/IWebhookClient';
import {INFRASTRUCTURE_IDENTIFIERS}             from '../../../infrastructure/InfrastructureModuleSymbols';
import {InvalidProviderError}                   from '../../common/errors/InvalidProviderError';
import {DOMAIN_APPLICATION_SERVICE_IDENTIFIERS} from '../../CoreModuleSymbols';
import {Provider}                               from '../../domain/Provider/Provider';

@injectable()
export class ProviderService {

  constructor(
    @inject(DOMAIN_APPLICATION_SERVICE_IDENTIFIERS.PROVIDER_SERVICE_CONFIG) public readonly providers: [Provider | any],
    @inject(INFRASTRUCTURE_IDENTIFIERS.WEBHOOK_CLIENT) public readonly webhook: IWebhookClient
  ) {}

  findProvider(name: string): Provider {
    return this.validateProvider(this.providers.find(provider => provider.name === name));
  }

  findProviders(names: [any]): [Provider] {
    return names.reduce((providers: [Provider], name: string) => {
      const provider: Provider = this.findProvider(name);
      providers.push(provider);
      return providers;
    }, []);
  }

  private validateProvider(provider: Provider): Provider {
    if (!provider || !provider.hasOwnProperty('name') || !provider.hasOwnProperty('endpoint')) {
      throw new InvalidProviderError(provider);
    }
    // return provider;
    return new Provider(provider.name, provider.endpoint);
  }
}
