import {Provider} from 'core/domain/Provider/Provider';

export default interface IProviderService {
  findProvider(name: string): Provider;
  findProviders(names: [any]): [Provider];
}
