import {inject, injectable}                     from 'inversify';
import IWebhookClient                           from '../../../../infrastructure/http/clients/Webhook/IWebhookClient';
import {IWebhookEvent}                          from '../../../../infrastructure/http/clients/Webhook/Event/IWebhookEvent';
import {INFRASTRUCTURE_IDENTIFIERS}             from '../../../../infrastructure/InfrastructureModuleSymbols';
import {DOMAIN_APPLICATION_SERVICE_IDENTIFIERS} from '../../../CoreModuleSymbols';
import {Provider}                               from '../../../domain/Provider/Provider';
import {ProviderService}                        from '../ProviderService';

@injectable()
export class ProviderWebhookService {

  constructor(
    @inject(DOMAIN_APPLICATION_SERVICE_IDENTIFIERS.PROVIDER_SERVICE) public readonly service: ProviderService,
    @inject(INFRASTRUCTURE_IDENTIFIERS.WEBHOOK_CLIENT) public readonly webhookClient: IWebhookClient
  ) {}

  create(names: [string], callbackUrl: string): any {
    const providers: [Provider | IWebhookEvent] = this.service.findProviders(names);
    const result                                = this.webhookClient.create(providers, callbackUrl);
    return result;
  }
}
