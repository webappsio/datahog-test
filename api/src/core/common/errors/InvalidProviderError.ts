import {Provider} from '../../domain/Provider/Provider';

export class InvalidProviderError extends Error {
  constructor(provider: Provider) {
    super(provider.name);
    this.name = 'InvalidProviderError';
  }
}
