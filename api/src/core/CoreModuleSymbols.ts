export const DOMAIN_APPLICATION_SERVICE_IDENTIFIERS = {
  PROVIDER_SERVICE        : Symbol.for('ProviderService'),
  PROVIDER_WEBHOOK_SERVICE: Symbol.for('ProviderWebhookService'),
  PROVIDER_SERVICE_CONFIG : Symbol.for('ProviderServiceConfig')
};
